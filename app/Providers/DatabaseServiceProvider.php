<?php

namespace Shockertunes\Providers;

use Illuminate\Support\ServiceProvider;

class DatabaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $path = base_path('modules');
        $this->loadMigrationsFrom([
            $path.'/Account/Migrations',
            $path.'/Post/Migrations'

        ]);
    }
}
