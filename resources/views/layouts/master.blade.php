<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>One Music - Modern Music HTML5 Template</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="css/shockertunes.css">

</head>

<body>
    <!-- Preloader -->
    @include ('layouts.preloader')

    <!-- ##### Header Area Start ##### -->
    
    @include ('layouts.header')

    <!-- ##### Header Area End ##### -->

    <!-- #### Main Content Start #### -->

    @yield ('content')

    <!-- #### Main Content End #### -->

    <!-- ##### Contact Area Start ##### -->
    
    @include ('layouts.contact')

    <!-- ##### Contact Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    
    @include ('layouts.footer')

    <!-- ##### Footer Area Start ##### -->

    <!-- ##### All Javascript Script ##### -->
    
    @include ('layouts.javascript')

</body>

</html>