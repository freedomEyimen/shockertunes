<?php

namespace Shockertunes\Modules;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class MainController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function successResponse($message)
    {
        return response()->json(['status' => 'Success', 'data' => $message]);
    }


    public function failedResponse($message)
    {
        return response()->json(['status' => 'Error', 'message' => $message]);
    }

}