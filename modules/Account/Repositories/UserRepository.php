<?php

namespace Shockertunes\Modules\Account\Repositories;

use Shockertunes\Modules\Account\Models\User;

class UserRepository
{

    public function store($data)
    {

        if(is_array($data))
        {
            $data = (object) $data;
        }
        $user = new User;
        
        $user->id = str_random(15);
        $user->first_name = $data->first_name;
        $user->last_name = $data->last_name;
        $user->email = $data->email;

        if($data->password !== $data->confirm_password)
        {
            return response()->json(['status' => 'error', 'message' => 'Passwords do not match']);
        }

        $user->password = bcrypt($data->password);

        $user->save();

        return $user;
    }



    public function show($id)
    {
        $user = User::where('id', $id)->first();

        return $user;
    }

}