<?php

namespace Shockertunes\Modules\Account\Controllers;

use Illuminate\Http\Request;
use Shockertunes\Modules\MainController;
use Shockertunes\Modules\Account\Repositories\UserRepository;

class UserController extends MainController
{

    public function __construct(UserRepository $userRepository)
    {
        $this->usersRepo = $userRepository;
    }
    
    public function store(Request $request)
    {
        $user = $this->usersRepo->store($request);

        if(!$user)
        {
            return $this->failedResponse('Sorry, there was an error storing this user');
        }

        return $this->successResponse($user);
    }


    public function show($id)
    {
        $user = $this->usersRepo->show($id);

        return $this->successResponse($user);
    }

}
